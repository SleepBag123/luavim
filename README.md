# Neovim Lua

__Hello !!,__
This is a small simple repository that I maintain in order to store by NeoVim configuration that I have rewritten in Lua. It is an attempt in order to recreate the features of my favourite GUI editor [Obsidian](https://obsidian.md) into NeoVim

NOTE : In order to use this config effectively please make sure that you are using NeoVim 0.5 +  

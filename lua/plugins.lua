require('nvim-autopairs').setup{}
local fn = vim.fn
local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
if fn.empty(fn.glob(install_path)) > 0 then
  fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
  vim.cmd 'packadd packer.nvim'
end
return require('packer').startup(function()
  -- Packer can manage itself
  use 'wbthomason/packer.nvim'
  use 'cocopon/iceberg.vim' -- Theme that I have come to love
  use 'vimwiki/vimwiki' -- For notes and stuff
  use {
  'nvim-telescope/telescope.nvim', -- For Fuzzy Finding Files
  requires = { {'nvim-lua/plenary.nvim'} }
  }
  use {"ellisonleao/glow.nvim"} -- Easy CLI markdown preview
  use 'windwp/nvim-autopairs'
  use 'freitass/todo.txt-vim'
  use {
  "folke/zen-mode.nvim",
  config = function()
    require("zen-mode").setup {
	    -- Configs (WIP)
    }
  end
  }
  use 'glepnir/dashboard-nvim'
end)

-- File Info:
-- Created By : Krishna Jani
-- Date : 27th of September 2021
-- Name : config.lua
-- Description : This is a meta file which is required in the init.lua file, and it basically sets some gloabl config options
--
local cmd = vim.cmd
local o = vim.o -- Options
local wo = vim.wo -- Window Option
local bo = vim.bo -- Buffer Option
local map = vim.api.nvim_set_keymap
-- Global Options

o.laststatus = 2
o.hlsearch = true
o.incsearch = true
o.ignorecase = true
wo.linebreak = true
wo.number = true
cmd("syntax on")
o.termguicolors = true

-- Colorscheme
cmd("colorscheme iceberg")
-- Keybindings
vim.g.mapleader = ' '
vim.g.maplocalleader = ' '
options = { noremap = true } --change the keybindings to only work in normal mode

map ('n', '<leader>fs', ':lua require"telescope.builtin".find_files({ hidden = true })<CR>', options)
map ('n', '<leader>ft', ':Lexplore<CR>', options)
map ('n', '<leader>p', ':Glow<CR>', options)
map ('n', '<leader>dc', ':silent w<bar>lua require("auto-pandoc").run_pandoc()<cr>', options) 
map ('n', '<leader>z', ':ZenMode', options)

-- Dashboard NeoVim
cmd("let g:dashboard_default_executive ='telescope'")
